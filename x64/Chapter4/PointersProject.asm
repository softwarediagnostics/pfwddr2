; ml64 /Zi PointersProject.asm /link /entry:main /SUBSYSTEM:CONSOLE

_data SEGMENT

a DWORD 0
b DWORD 0

_data ENDS

_text SEGMENT

main PROC

	lea rax, a
	mov dword ptr [rax], 1
 
	lea rbx, b
	mov dword ptr [rbx], 1

	mov eax, dword ptr [rax]
	add dword ptr [rbx], eax

	inc eax

	imul eax, dword ptr [rbx]
	mov dword ptr [rbx], eax

	ret

main ENDP

_text ENDS

END