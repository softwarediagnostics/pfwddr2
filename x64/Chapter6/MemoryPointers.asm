; ml64 /Zi MemoryPointers.asm /link /entry:main /SUBSYSTEM:CONSOLE

_data SEGMENT

a  DWORD 0
b  DWORD 0
pa QWORD 0
pb QWORD b

_data ENDS

_text SEGMENT

main PROC

	lea rax, a
	mov [pa], rax

	mov rax, [pa]
	mov dword ptr [rax], 1

	mov rbx, [pb]
	mov dword ptr [rbx], 1

	mov ecx, dword ptr [rax]
	add ecx, dword ptr [rbx]

	mov dword ptr [rbx], ecx

	ret

main ENDP

_text ENDS

END